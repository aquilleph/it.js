/**
 * Created by acsherrock on 1/26/15.
 */

(function(root) {
    /* Establish the root object:
     *    `window` in the browser.
     *    `exports` on the server. */
    var _ = root;


    /* Create a safe reference to the IT object for use below */
    var it = function(obj) {
        if (obj instanceof it)
            return obj;
        if (!(this instanceof it))
            return new it(obj);
        this._wrap = obj;
    };


    /* Current it utils version */
    it.VERSION = '0.0.0';


    var n_isArray = Array.isArray;
    var hasProperty = Object.prototype.hasOwnProperty;
    var toStr = Object.prototype.toString;
    var keysOf = Object.keys;


    it.addEventListener = function (element, eventName, callback) {
        if (it.isString(element)) {
            var elementStr = element;

            if (elementStr.charAt(0) === '.') {
                element = document.querySelectorAll(elementStr);
            } else if (elementStr.charAt(0) === '#') {
                element = document.querySelectorAll(elementStr);
            } else {
                element = document.getElementsByClassName(elementStr);
                if (!it.isDomNodeList(element)) {
                    element = document.getElementById(elementStr);
                }
            }
        }

        if (it.isDomNodeList(element) || it.isArray(element)) {
            for (var index = 0; index < element.length; ++index) {
                element[index].addEventListener(eventName, callback);
            }
        } else if (it.isDomNode(element)) {
            element.addEventListener(eventName, callback);
        } else {
            console.log('Error finding elements to add event listeners.');
        }
    };

    /**
     * Check if an object is undefined.
     * @param obj
     * @returns {boolean}
     */
    it.isUndefined = function (obj) {
        return obj === void null;
    };



    /**
     * Check if an object is defined.
     * @param obj
     * @returns {boolean}
     */
    it.isDefined = function (obj) {
        return obj !== void null;
    };



    /**
     * Wrapper for Object.prototype.hasOwnProperty.
     * @param obj
     * @param property
     */
    it.has = function (obj, property) {
        return obj != null && hasProperty.call(obj, property);
    };



    /**
     * Check if given variable is an object.
     * @param obj
     * @returns {boolean}
     */
    it.isObject = function(obj) {
        var type = typeof obj;
        return type === 'function' || type === 'object' && !!obj;
    };



    /**
     * Check if given variable is an array.
     * @param obj
     * @returns {boolean}
     */
    it.isArray = n_isArray || function(obj) {
        return toStr.call(obj) === '[object Array]';
    };



    /**
     * Check if given variable is a DOM node element.
     * @param obj
     * @returns {boolean}
     */
    it.isDomNode = function (obj) {
        return !!(obj && obj.nodeType === 1);
    };



    /**
     * Check if given variable is a DOM nodeList.
     * @param obj
     * @returns {boolean}
     */
    it.isDomNodeList = function (obj) {
        var nodeListRegex = /^\[object (HTMLCollection|NodeList|Object)]$/;
        return typeof obj === 'object'
            && nodeListRegex.test(toStr.call(obj))
            && hasProperty.call(obj, 'length')
            && (obj.length === 0 || (typeof  obj[0] === 'object' && obj[0].nodeType > 0));
    };



    /**
     * Check if given variable is a function.
     * @param obj
     * @returns {boolean}
     */
    it.isFunction = function (obj) {
        return toStr.call(obj) === '[object Function]';
    };



    /**
     * Check if given variable is a string.
     * @param obj
     * @returns {boolean}
     */
    it.isString = function (obj) {
        return toStr.call(obj) === '[object String]';
    };



    /**
     * Set default value of variables.
     * @param argument
     * @param defaultValue
     * @returns {*}
     */
    it.default = function (argument, defaultValue) {
        return it.isDefined(argument)
            ? argument
            : defaultValue;
    };


    /**
     * Append properties to an object. Cleans up simple inheritance.
     * @param dest - the object or prototype that you want to append properties.
     * @returns {*}
     */
    it.extend = function(dest) {
        if (!it.isObject(dest))
            return dest;

        for (var i = 1; i < arguments.length; ++i) {
            var src = arguments[i];
            for (var prop in src) {
                if (hasProperty.call(src, prop)) {
                    dest[prop] = src[prop];
                }
            }
        }

        return dest;
    };



    /**
     * Encode objects, arrays, and strings for URLs.
     * @param data
     * @returns {string}
     */
    it.urlencode = function (data) {
        var encoded = '';
        var i = 0;

        if (it.isObject(data)) {
            var keys = keysOf(data);
            for (i = 0; i < keys.length; ++i) {
                if (i > 0) encoded += '&';
                encoded += encodeURIComponent(keys[i]) + '=' + encodeURIComponent(data[keys[i]]);
            }
        } else if (it.isArray(data)) {
            for (i = 0; i < data.length; ++i) {
                if (i > 0) encoded += '&';
                encoded += encodeURIComponent(data[i]);
            }
        } else if (it.isString(data)) {
            encoded = encodeURIComponent(data);
        }

        return encoded;
    };


    it.ajax = {};
    it.ajax.options = {}; // TODO: implement ajax setup a la jQuery
    it.ajax.versions = [
        "MSXML2.XmlHttp.5.0",
        "MSXML2.XmlHttp.4.0",
        "MSXML2.XmlHttp.3.0",
        "MSXML2.XmlHttp.2.0",
        "Microsoft.XmlHttp"
    ];

    it.ajax.x = function() {
        var RequestType = (_.XMLHttpRequest)
            ? XMLHttpRequest
            : ActiveXObject;

        for (var i = 0; i < it.ajax.versions.length; i++) {
            try {
                return new RequestType(it.ajax.versions[i]);
            } catch (e) {
                console.log(e);
            }
        }
    };

    it.ajax.get = function(url, options) {
        it.ajax.send(url, options, 'GET');
    };

    it.ajax.post = function(url, options) {
        it.ajax.send(url, options, 'POST');
    };

    it.ajax.send = function (url, options, method) {
        if (typeof url === "object") {
            options = url;
            url = undefined;
        }

        // force args to be an object.
        options = options || {};

        // Get the request object.
        var x = it.ajax.x();

        var data     = options.data || {},
            complete = options.complete || function(){},
            success  = options.success || complete,
            error    = options.error || complete,
            async    = options.async || true;
            method   = method || options.method || 'POST';
            url      = url || options.url;

        // URL encode data.
        var encodedData = it.urlencode(data);

        // set query value for POST requests.
        var body = (method === 'POST')
            ? encodedData
            : null;

        if (method === 'GET') {
            url += '?' + encodedData;
        }


        x.open(method, url, async);
        x.onreadystatechange = function() {
            if (x.readyState == XMLHttpRequest.DONE) {
                if (x.status >= 200 && x.status < 300) {
                    success(x.response, x.status, x.statusText, x);
                } else if (x.status >= 300) {
                    error(x.response, x.status, x.statusText, x);
                } else {
                    complete(x.response, x.status, x.statusText, x);
                }
            }
        };

        if (method == 'POST') {
            x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }

        x.send(body)
    };



    // Create aliases
    it.post = it.ajax.post;
    it.get  = it.ajax.get;



    /* Export it object for Node.js. Or add `it` as a global object if in a browser. */
    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined') {
            exports = module.exports = it;
        }

        exports.it = it;
    } else {
        _.it = it;
    }
}(window));